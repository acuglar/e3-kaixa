import os
import json
from datetime import date

# filename = 'transactions.json'

def all_transactions(filename):
    file_exists = os.path.exists(filename)

    if not file_exists: 
        return []
    else: 
        file_size = os.stat(filename).st_size
        if file_size == 0:
            return []        
        else:
            current_file = open(filename, 'r')
            data = json.load(current_file)
            current_file.close()
            return data 

def post_transaction(filename, title, transaction_type, value):
    transactions = all_transactions(filename)       
    current_date = date.today().strftime('%d/%m/%Y') 
    transaction = {
        'title': title.capitalize(),
        'transaction_type': transaction_type.lower(),
        'value': int(value),
        'date': current_date
    }
    
    current_file = open(filename, 'w+')
    transactions.append(transaction)
    json.dump(transactions, current_file, indent=2)
    current_file.close()
    return transaction

def calculate_balance(filename):
    transactions = all_transactions(filename)       
    balance = 0

    if transactions:
        for client in transactions:
            if client['transaction_type'] == 'income':
                balance += client['value']
            else:
                balance -= client['value']
        return f'Seu saldo é de: R$ {balance}'
    else:
        return []
